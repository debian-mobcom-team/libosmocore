#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

include /usr/share/dpkg/pkg-info.mk

CHANGELOG_DATE ?= $(shell LC_ALL=C date -u -d @$(SOURCE_DATE_EPOCH) +"%d %B %Y")
export DEB_BUILD_MAINT_OPTIONS = hardening=+all
arch = $(shell dpkg-architecture -qDEB_BUILD_ARCH)

export DEB_LDFLAGS_MAINT_STRIP = -Wl,-Bsymbolic-functions

%:
	dh $@

# 20171109, ta, not needed anymore
#override_dh_strip:
#	dh_strip --dbg-package=libosmocore-dbg

override_dh_install:
#	sed -i "/dependency_libs/ s/'.*'/''/" `find . -name '*.la'`
	find debian/tmp -name '*.la' -delete
	# package libosmosub not created, remove corresponding links/files
	rm debian/tmp/usr/lib/*/libosmousb.so
	rm debian/tmp/usr/lib/*/libosmousb.so.0
	rm debian/tmp/usr/lib/*/pkgconfig/libosmousb.pc
	dh_install

# Print test results in case of a failure
override_dh_auto_test:
	-dh_auto_test || (find . -name testsuite.log -exec cat {} \; ; false)

override_dh_installman:
	cd debian/man ; CHANGELOG_DATE="$(CHANGELOG_DATE)" ./genmanpages.sh
	dh_installman

override_dh_missing:
	dh_missing --list-missing

override_dh_clean:
	dh_clean
	$(RM) .version
	$(RM) debian/man/osmo-arfcn.1
	$(RM) debian/man/osmo-auc-gen.1
	$(RM) include/osmocom/core/bit16gen.h
	$(RM) include/osmocom/core/bit32gen.h
	$(RM) include/osmocom/core/bit64gen.h
	$(RM) include/osmocom/core/crc16gen.h
	$(RM) include/osmocom/core/crc32gen.h
	$(RM) include/osmocom/core/crc64gen.h
	$(RM) include/osmocom/core/crc8gen.h
	$(RM) include/osmocom/core/socket_compat.h
	$(RM) src/core/crc16gen.c
	$(RM) src/core/crc32gen.c
	$(RM) src/core/crc64gen.c
	$(RM) src/core/crc8gen.c
	$(RM) src/crc16gen.c
	$(RM) src/crc32gen.c
	$(RM) src/crc64gen.c
	$(RM) src/crc8gen.c
	$(RM) tests/package.m4
	$(RM) tests/testsuite
	$(RM) -r utils/__pycache__
	$(RM) -r doc/codec/
	$(RM) -r doc/core/
	$(RM) -r doc/ctrl/
	$(RM) -r doc/gsm/
	$(RM) -r doc/vty/html/
	$(RM) -r doc/vty/latex/
	$(RM) utils/conv_codes_gsm.pyc
	$(RM) utils/conv_gen.pyc

override_dh_installchangelogs:
	dh_installchangelogs debian/changelog_upstream
